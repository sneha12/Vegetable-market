package com.example.akanksha.sn;

/**
 * Created by akanksha on 4/22/2018.
 */

public class Fruit {
    private int id;
    private String name;
    private String price;
    private byte[] image;

    public Fruit(String name, String price, byte[] image, int id) {

        this.name = name;
        this.price = price;
        this.image = image;
        this.id = this.id;

    }
    public int getId() {
        return id;
    }

    public void setId() {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice() {
        this.price = price;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName() {
        this.name = name;
    }

}
