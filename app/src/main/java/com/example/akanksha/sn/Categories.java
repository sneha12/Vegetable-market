package com.example.akanksha.sn;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

    public class Categories extends AppCompatActivity {

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_categories);

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            ImageButton ibtn = (ImageButton) findViewById ( R . id . imageButton );
            ibtn . setImageResource ( R . drawable .fruits4 );
            ImageButton ibtn2 = ( ImageButton ) findViewById ( R . id . imageButton2 );
        // perform click event on button's
            ibtn. setOnClickListener ( new View. OnClickListener () {
                @Override
                public void onClick ( View view ) {
                    Intent i=new Intent(com.example.akanksha.sn.Categories.this,select_fruit.class);
                    startActivity(i);
                }
            });
            ibtn2 . setOnClickListener ( new View . OnClickListener () {
                @Override
                public void onClick ( View view ) {
                    Intent i=new Intent(com.example.akanksha.sn.Categories.this,select_veggies.class);
                    startActivity(i);
                }
            });
        }
    }

