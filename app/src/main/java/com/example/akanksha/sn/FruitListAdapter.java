package com.example.akanksha.sn;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by gadhekar on 3/25/2018.
 */

public class FruitListAdapter extends BaseAdapter {
    private Context context;
    private int layout;
    private ArrayList<Fruit> fruitsList;

    public FruitListAdapter(Context context, int layout, ArrayList<Fruit> foodsList) {
        this.context = context;
        this.layout = layout;
        this.fruitsList = foodsList;

    }
        @Override
        public int getCount () {
            return fruitsList.size();
        }

        @Override
        public Object getItem ( int position){
            return fruitsList.get(position);
        }

        @Override
        public long getItemId ( int position){
            return position;
        }

        private class ViewHolder {
            ImageView imageView;
            TextView textName, textPrice;

        }

        @Override
        public View getView ( int position, View view, ViewGroup viewGroup){
            View row = view;
            ViewHolder holder = new ViewHolder();
            if (row == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(layout, null);
                holder.textName = (TextView) row.findViewById(R.id.name);
                holder.textPrice = (TextView) row.findViewById(R.id.price);
                holder.imageView = (ImageView) row.findViewById(R.id.imgFruit);
                row.setTag(holder);
            } else {
                holder = (ViewHolder) row.getTag();

            }
            Fruit fruit = fruitsList.get(position);
            holder.textName.setText(fruit.getName());
            holder.textPrice.setText(fruit.getPrice());


            byte[] foodImage = fruit.getImage();
            Bitmap bitmap = BitmapFactory.decodeByteArray(foodImage, 0, foodImage.length);
            holder.imageView.setImageBitmap(bitmap);
            return row;
        }
    }
