package com.example.akanksha.sn;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

/**
 * Created by gadhekar on 3/25/2018.
 */

class SQLiteHelper extends SQLiteOpenHelper {



    public SQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    public void insertData(String name,String price,byte[] image) {
        SQLiteDatabase database = getWritableDatabase();
        String sql = "INSERT INTO Fruit VALUES(NULL,?,?,?)";
        SQLiteStatement statement = database.compileStatement(sql);
        statement.clearBindings();

        statement.bindString(1, name);
        statement.bindString(2, price);
        statement.bindBlob(3,image);
       // statement.bindString(3, String.valueOf(image));
        statement.executeInsert();
    }

    public Cursor getData(String sql){
        SQLiteDatabase database=getWritableDatabase();
        return database.rawQuery(sql,null);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void queryData(String s) {
        SQLiteDatabase database=getWritableDatabase();
        database.execSQL(s);
    }
}
