package com.example.akanksha.sn;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by akanksha on 4/22/2018.
 */

public class veggiesList extends AppCompatActivity {
    ListView lv;
    //GridView gridView;
    ArrayList<Fruit> list;
    FruitListAdapter adapter=null;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.veggieslist);

       lv= (ListView) findViewById(R.id.listview);
        list=new ArrayList<>();
        adapter=new FruitListAdapter(this,R.layout.fruit_item,list);
        lv.setAdapter(adapter);
        //lv.setAdapter(adapter);
        //get aLL DATA SQLLITE
        Cursor cursor=add_product.sqLiteHelper.getData("SELECT * FROM Fruit");
        list.clear();
        while (cursor.moveToNext()){
            int id=cursor.getInt(0);
            String name=cursor.getString(1);
            String price=cursor.getString(2);
            byte[] image=cursor.getBlob(3);
            list.add(new Fruit(name,price,image,id));
        }
        adapter.notifyDataSetChanged();
    }
}
