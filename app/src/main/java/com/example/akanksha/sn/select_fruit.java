package com.example.akanksha.sn;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

public class select_fruit extends AppCompatActivity implements AdapterView.OnItemSelectedListener{
    TextView display_data,display_data2;
    String[] kgs1={"1","2","3","4"};
    String[] kgs2={"1","2","3","4"};
    String[] kgs3={"1","2","3","4"};
    String[] kgs4={"1","2","3","4"};
    String[] kgs5={"1","2","3","4"};

    String c="";
    String tr="",tr1="",tr2="",tr3="",tr4="",tr5="";
    TextView appler,bananar,lycheer,mangor,oranger;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_fruit);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        display_data = (TextView) findViewById(R.id.display_result);
        display_data2 = (TextView) findViewById(R.id.display_result2);
        appler = (TextView) findViewById(R.id.applerate);
        bananar = (TextView) findViewById(R.id.bananarate);
        lycheer = (TextView) findViewById(R.id.lycheerate);
        mangor = (TextView) findViewById(R.id.mangorate);
        oranger = (TextView) findViewById(R.id.orangerate);

         Spinner sp1 = (Spinner) findViewById(R.id.s1);
        Spinner sp2 = (Spinner) findViewById(R.id.ss1);
        Spinner sp3 = (Spinner) findViewById(R.id.ss2);
        Spinner sp4 = (Spinner) findViewById(R.id.ss3);
        Spinner sp5 = (Spinner) findViewById(R.id.ss4);


        final ArrayAdapter aa1 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, kgs1);
        aa1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp1.setAdapter(aa1);
        sp1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                c = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp2.setOnItemSelectedListener(this);
        ArrayAdapter aa2 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, kgs2);
        aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp2.setAdapter(aa2);
        sp2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                c = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ArrayAdapter aa3 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, kgs3);
        aa3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp3.setAdapter(aa3);
        sp3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                c = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        ArrayAdapter aa4 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, kgs4);
        aa4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp4.setAdapter(aa4);
        sp4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                c = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ArrayAdapter aa5 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, kgs5);
        aa5.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp5.setAdapter(aa5);
        sp5.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                c = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public void func()
    {
        int a= Integer.parseInt(c);
        String d=appler.getText().toString();
        Double b= Double.parseDouble(d);
        Double finl= Double.valueOf((a*b));
        //return finl;
        tr=String.valueOf(finl);
    }

    public void func1()
    {
        int aa= Integer.parseInt(c);
        String bb=bananar.getText().toString();
        Double cc= Double.valueOf(bb);
        Double sa= Double.valueOf((aa*cc));
        tr1=String.valueOf(sa);
    }

    public void func2()
    {
        int aa= Integer.parseInt(c);
        String bb=lycheer.getText().toString();
        Double cc= Double.valueOf(bb);
        Double sa= Double.valueOf((aa*cc));
        tr2=String.valueOf(sa);
    }

    public void func3()
    {
        int aa= Integer.parseInt(c);
        String bb=mangor.getText().toString();
        Double cc= Double.valueOf(bb);
        Double sa= Double.valueOf((aa*cc));
        tr3=String.valueOf(sa);
    }
    public void func4()
    {
        int aa= Integer.parseInt(c);
        String bb=oranger.getText().toString();
        Double cc= Double.valueOf(bb);
        Double sa= Double.valueOf((aa*cc));
        tr4=String.valueOf(sa);
    }
    public void addtomato(View view){
        func();
        Intent i=new Intent(select_fruit.this,Shopping_Cart.class);
        i.putExtra("a",tr);
        i.putExtra("v",tr1);
        i.putExtra("w",tr2);
        i.putExtra("x",tr3);
        i.putExtra("y",tr4);
        startActivity(i);
    }

    public void addbanana(View view)
    {
        func1();
        Intent i=new Intent(select_fruit.this,Shopping_Cart.class);
        i.putExtra("a",tr);
        i.putExtra("v",tr1);
        i.putExtra("w",tr2);
        i.putExtra("x",tr3);
        i.putExtra("y",tr4);
        startActivity(i);
    }
    public  void addpotato(View view){
        func2();
        Intent i=new Intent(select_fruit.this,Shopping_Cart.class);
        i.putExtra("a",tr);
        i.putExtra("v",tr1);
        i.putExtra("w",tr2);
        i.putExtra("x",tr3);
        i.putExtra("y",tr4);
        startActivity(i);
    }
    public void addlemon(View view){
        func3();
        Intent i=new Intent(select_fruit.this,Shopping_Cart.class);
        i.putExtra("a",tr);
        i.putExtra("v",tr1);
        i.putExtra("w",tr2);
        i.putExtra("x",tr3);
        i.putExtra("y",tr4);
        startActivity(i);
    }
    public void addorange(View view){
        func4();
        Intent i=new Intent(select_fruit.this,Shopping_Cart.class);
        i.putExtra("a",tr);
        i.putExtra("v",tr1);
        i.putExtra("w",tr2);
        i.putExtra("x",tr3);
        i.putExtra("y",tr4);
        startActivity(i);
    }
}
