package com.example.akanksha.sn;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class Login extends AppCompatActivity {
    Button b1,b2;
    EditText e1,e2;
    DatabaseHelper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        db=new DatabaseHelper(this);
        e1=(EditText)findViewById(R.id.editText);
        e2=(EditText)findViewById(R.id.editText2);
        b1=(Button)findViewById(R.id.button);
        b2=(Button)findViewById(R.id.register);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email=e1.getText().toString();
                String password=e2.getText().toString();
                Boolean chkemailpass=db.emailpassword(email,password);
                if ((email.equals("admin")) && (password.equals("123")))
                {

                    Toast.makeText(getApplicationContext(),"login successful as a admin",Toast.LENGTH_LONG).show();
                    Intent g=new Intent(Login.this,adminn.class);
                    startActivity(g);
                }
                else if(chkemailpass==true){

                    Toast.makeText(getApplicationContext(),"login successful",Toast.LENGTH_LONG).show();
                    Intent g=new Intent(Login.this,Categories.class);
                    startActivity(g);

                }

                else{
                    Toast.makeText(getApplicationContext(),"Wrong email or password",Toast.LENGTH_LONG).show();
                }
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Login.this,Register.class);
                startActivity(i);
            }
        });
    }
}
