package com.example.akanksha.sn;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

public class select_veggies extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    final String[] kgs1={"1","2","3","4"};
    final  String[] kgs2={"1","2","3","4"};
    String[] kgs3={"1","2","3","4"};
    String[] kgs4={"1","2","3","4"};
    String[] kgs5={"1","2","3","4"};
    String c="";
    String c1="";
    String tr="",tr1="",tr2="",tr3="",tr4="",tr5="";
TextView tomator,onionrate,potatorate,lemonr,cabbager;
Button b1;
  @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_veggies);
        b1=(Button)findViewById(R.id.applebtn);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tomator =(TextView)findViewById(R.id.tomatorate);
        onionrate =(TextView)findViewById(R.id.onionrate);
      potatorate =(TextView)findViewById(R.id.potatorate);
      lemonr =(TextView)findViewById(R.id.lemonrate);
      cabbager =(TextView)findViewById(R.id.cabbagerate);

        Spinner sp1= (Spinner)findViewById(R.id.s6);
        Spinner sp2= (Spinner)findViewById(R.id.ss1);
        Spinner sp3= (Spinner)findViewById(R.id.s8);
        Spinner sp4= (Spinner)findViewById(R.id.ss3);
        Spinner sp5= (Spinner)findViewById(R.id.ss4);


        ArrayAdapter aa1 = new ArrayAdapter ( this , android . R . layout . simple_spinner_item , kgs1 );
        aa1 . setDropDownViewResource ( android . R . layout . simple_spinner_dropdown_item );
        sp1. setAdapter ( aa1 );

      sp1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
          @Override
          public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

              c=adapterView.getItemAtPosition(i).toString();


          }

          @Override
          public void onNothingSelected(AdapterView<?> adapterView) {

          }
      });

        ArrayAdapter aa2 = new ArrayAdapter ( this , android . R . layout . simple_spinner_item , kgs2 );
     aa2. setDropDownViewResource ( android . R . layout . simple_spinner_dropdown_item );
        sp2. setAdapter ( aa2 );
      sp2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
          @Override
          public void onItemSelected(AdapterView<?> adapterView, View view, int ip, long l) {
              c1=adapterView.getItemAtPosition(ip).toString();
          }
          @Override
          public void onNothingSelected(AdapterView<?> adapterView) {
                                        }
      });

              sp3.setOnItemSelectedListener(this);
        ArrayAdapter aa3= new ArrayAdapter ( this , android . R . layout . simple_spinner_item , kgs3 );
        aa3. setDropDownViewResource ( android . R . layout . simple_spinner_dropdown_item );
        sp3. setAdapter ( aa3 );
        sp3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                c1=parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp4.setOnItemSelectedListener(this);
        ArrayAdapter aa4= new ArrayAdapter ( this , android . R . layout . simple_spinner_item , kgs4 );
        aa4. setDropDownViewResource ( android . R . layout . simple_spinner_dropdown_item );
        sp4. setAdapter ( aa4 );
        sp4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                c1=parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp5.setOnItemSelectedListener(this);
        ArrayAdapter aa5= new ArrayAdapter ( this , android . R . layout . simple_spinner_item , kgs5 );
        aa5. setDropDownViewResource ( android . R . layout . simple_spinner_dropdown_item );
        sp5. setAdapter ( aa5 );
        sp5.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                c1=parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public void func()
    {
        int a= Integer.parseInt(c);
        String d=tomator.getText().toString();
        Double b= Double.parseDouble(d);
        Double finl= Double.valueOf((a*b));
        //return finl;
         tr=String.valueOf(finl);
    }

    public void func1()
    {
       int aa= Integer.parseInt(c1);
       String bb=onionrate.getText().toString();
       Double cc= Double.valueOf(bb);
       Double sa= Double.valueOf((aa*cc));
       tr1=String.valueOf(sa);
    }

    public void func2()
    {
        int aa= Integer.parseInt(c1);
        String bb=potatorate.getText().toString();
        Double cc= Double.valueOf(bb);
        Double sa= Double.valueOf((aa*cc));
        tr2=String.valueOf(sa);
    }

    public void func3()
    {
        int aa= Integer.parseInt(c1);
        String bb=lemonr.getText().toString();
        Double cc= Double.valueOf(bb);
        Double sa= Double.valueOf((aa*cc));
        tr3=String.valueOf(sa);
    }
    public void func4()
    {
        int aa= Integer.parseInt(c1);
        String bb=cabbager.getText().toString();
        Double cc= Double.valueOf(bb);
        Double sa= Double.valueOf((aa*cc));
        tr4=String.valueOf(sa);
    }
    public void calltomato(View view){
      func();
      Intent i=new Intent(select_veggies.this,Shopping_Cart.class);
      i.putExtra("k",tr);
        i.putExtra("s",tr1);
        i.putExtra("n",tr2);
        i.putExtra("e",tr3);

        i.putExtra("h",tr4);
      startActivity(i);
    }

    public void addtoonion(View view)
    {
        func1();
        Intent i=new Intent(select_veggies.this,Shopping_Cart.class);
        i.putExtra("s",tr1);
        i.putExtra("k",tr);
        i.putExtra("n",tr2);
        i.putExtra("e",tr3);
        i.putExtra("h",tr4);
        startActivity(i);
    }
    public  void addpotato(View view){
        func2();
        Intent i=new Intent(select_veggies.this,Shopping_Cart.class);
        i.putExtra("k",tr);
        i.putExtra("s",tr1);
        i.putExtra("n",tr2);
        i.putExtra("e",tr3);
        i.putExtra("h",tr4);
        startActivity(i);
    }
    public void addlemon(View view){
        func3();
        Intent i=new Intent(select_veggies.this,Shopping_Cart.class);
        i.putExtra("k",tr);
        i.putExtra("s",tr1);
        i.putExtra("n",tr2);
        i.putExtra("e",tr3);
        i.putExtra("h",tr4);
        startActivity(i);
    }
    public void addcabbage(View view){
        func4();
        Intent i=new Intent(select_veggies.this,Shopping_Cart.class);
        i.putExtra("k",tr);
        i.putExtra("s",tr1);
        i.putExtra("n",tr2);
        i.putExtra("e",tr3);
        i.putExtra("h",tr4);
        startActivity(i);
    }

}
